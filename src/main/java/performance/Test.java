package performance;

/**
 * Created by Vince on 05-05-17.
 */
public class Test {
    @MeasurePerformance
    public void sayHello(String name) {
        System.out.println("hello" + name);
        doThis();
    }

    @MeasurePerformance
    private void doThis() {
        System.out.println("ha");
    }
}
