package performance;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class MeasurePerformanceAspect {

    @Around("@annotation(MeasurePerformance) && execution(@MeasurePerformance * *.*(..))")
    public void measurePerformanceAnnotationAspect(final ProceedingJoinPoint joinPoint) throws Throwable {
        final long startTime = System.currentTimeMillis();
        final String methodName = joinPoint.getSignature().getName();
        joinPoint.proceed();
        final long endTime = System.currentTimeMillis();
        System.out.println("Method " + methodName + " took " + (endTime - startTime) + " ms");
    }
}
