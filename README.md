# Measure Performance

Created @MeasurePerformance annotation that can be put on methods that you want to measure performance for.

Performance is measured in milliseconds and then logged to console.